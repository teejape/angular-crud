const express = require("express");
const router = express.Router();
const tasksModel = require('./tasksModel');
const root = "./";

router.get('/tasks', (req, res, next) => {
    res.sendFile('dist/index.html', {root});
});


router.get('/api/tasks', (req, res, next) => {
    tasksModel.getTasks(req, res);
});

router.post('/api/add-task', (req, res) => {
    tasksModel.addTask(req, res);
});

router.post('/api/edit-task', (req, res) => {
    tasksModel.editTask(req, res);
});

module.exports = router;