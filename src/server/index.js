const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const routes = require('./routes');
const dbConnection = require('./dbConnection');

dbConnection.connect();

const root = "./";
const port = 3000;

const app = express();

app.use(bodyParser.json({type: "*/json"}));

app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(root, 'dist')));

app.use("/", routes);

app.get('*', (req, res) => {
    res.status(200).sendFile('dist/index.html', {root});
});

app.listen(port, () => {console.log('server live')});