const dbConnection = require('./dbConnection');

function getTasks(req, res, next) {
    dbConnection.query('SELECT * FROM `tasks`', (error, results, fields) => {
        console.log(results);
        return res.json(results);
    });
}

function addTask (req, res) {
    let newTask = {
        name: req.body.name,
        description: req.body.description
    };
    let query = `INSERT INTO tasks (name, description) VALUES ('${newTask.name}', '${newTask.description}')`;
    dbConnection.query(query, (error, result) => {
        if (error) {
            console.log(`SQL QUERY: ${query}`);
            throw new Error(error);
        }
        return res.status(200).send({"status":"added task"});
    });
}

function editTask (req, res) {
    let editTask = {
        id: req.body.id,
        name: req.body.name,
        description: req.body.description
    };
    let query = `UPDATE tasks SET name='${editTask.name}', description='${editTask.description}' WHERE id=${editTask.id}`;
    dbConnection.query(query, (error, result) => {
        if (error) {
            console.log(`SQL QUERY: ${query}`);
            throw new Error(error);
        }
        return res.status(200).send({"status":"updated task " + req.body.id});
    });
}

module.exports = {
    getTasks,
    addTask,
    editTask
};
