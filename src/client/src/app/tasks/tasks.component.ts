import { Component, OnInit } from '@angular/core';
import {NgModel} from "@angular/forms";
import {Task} from "./task";
import {FormBuilder, FormGroup} from "@angular/forms";
import {TaskService} from "../task-service.service";
import {catchError, map} from "rxjs/operators";


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks: any = [];
  selectedTask: Task;
  addForm: FormGroup;
  editForm: FormGroup;

  constructor(private taskService: TaskService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getTasks();
    this.initAddForm();
    this.initEditForm();
  }

  initAddForm() {
    this.addForm = this.formBuilder.group({
      name: [null],
      description: [null]
    });
  }

  initEditForm() {
    this.editForm = this.formBuilder.group({
      id: [null],
      name: [null],
      description: [null]
    });
  }

  onEditTask() {
    let data = JSON.stringify(this.editForm.getRawValue());
    this.editTask(data);
  }

  onAddTask() {
    let data = JSON.stringify(this.addForm.getRawValue());
    this.addTask(data);
  }

  getTasks() {
    return this.taskService.getTasks().subscribe(tasks =>
    {
      this.tasks = tasks;
    });
  }

  updateTasksList() {
    this.taskService.updateTasksList().subscribe(tasks => {
      this.tasks = tasks;
      console.log(this.tasks);
    })
  }

  editTask(task) {
    return this.taskService.editTask(task).subscribe(
      ()=>{
        this.updateTasksList();
      }
    )
  }

  addTask(task) {
    return this.taskService.addTask(task).subscribe(
      ()=>{
        this.updateTasksList();
      }
    )
  }

  onSelect(task: Task) {
    this.selectedTask = task;
  }
}
