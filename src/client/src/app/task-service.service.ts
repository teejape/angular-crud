import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Task} from "./tasks/task";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(private http: HttpClient) { }

  getTasks(): Observable<any>{
    return this.http.get('/api/tasks');
  }
  updateTasksList(): Observable<any>{
    return this.http.get('/api/tasks');
  }
  addTask(task) {
    return this.http.post('/api/add-task', task, this.httpOptions)
  }
  editTask(task) {
    return this.http.post('/api/edit-task', task, this.httpOptions)
  }

}
